from flask import Flask, render_template, send_file
from PIL import Image
import io
import random

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/overlay')
def overlay():
    # Generate a random colored image
    img = Image.new('RGBA', (256, 256), (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), 127))
    img_io = io.BytesIO()
    img.save(img_io, 'PNG', quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/png')

if __name__ == '__main__':
    app.run(debug=True)

