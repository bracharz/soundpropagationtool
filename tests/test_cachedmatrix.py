import pytest
import polars as pl
from soundpropagationtools.cachedmatrix import *


def test_empty_df_get_entries():
    cdf = CachedMatrix()
    empty_df = pl.DataFrame({'x': ['x1'], 'y': ['y1']})
    with pytest.raises(ValueError) as e:
        cdf.get_df_entries(empty_df)
    assert str(e.value) == "Missing entries, please integrate these first."

    cdf = CachedMatrix()
    df = pl.DataFrame({
        'x': ['x1', 'x2', 'x1', 'x2'],
        'y': ['y1', 'y1', 'y2', 'y2'],
        'val': [1.1, 2, 3, 4]})
    cdf.integrate_df(df)
    query_df = pl.DataFrame({
        'x': ['x1', 'x2', 'x1', 'x2'],
        'y': ['y1', 'y1', 'y2', 'y2']})
    result = cdf.get_df_entries(query_df)
    assert result.equals(
        pl.DataFrame({
            'x': ['x1', 'x1', 'x2', 'x2'],
            'y': ['y1', 'y2', 'y1', 'y2'],
            'val': [1.1, 3, 2, 4]}).sort(['x', 'y'])
    )
    query_df = pl.DataFrame({
        'x': ['x1', 'x2'],
        'y': ['y1', 'y1']})
    result = cdf.get_df_entries(query_df)
    assert result.equals(
        pl.DataFrame({
            'x': ['x1',  'x2'],
            'y': ['y1', 'y1'],
            'val': [1.1, 2]}).sort(['x', 'y'])
    )


def test_purge_entries():
    df = pl.DataFrame({
        'x': ['x1'],
        'y': ['y1'],
        'val': [1.1]})
    cdf = CachedMatrix(df=df)
    cdf.purge_df()
    query_df = pl.DataFrame({'x': ['x1'], 'y': ['y1']})
    with pytest.raises(ValueError) as e:
        cdf.get_df_entries(query_df)
    assert str(e.value) == "Missing entries, please integrate these first."


def test_update_no_overlap():
    cdf = CachedMatrix()
    df = pl.DataFrame({
        'x': ['x1', 'x2', 'x1', 'x2'],
        'y': ['y1', 'y1', 'y2', 'y2'],
        'val': [1.1, 2, 3, 4]})
    cdf.integrate_df(df)
    new_df = pl.DataFrame({
        'x': ['x3', 'x4'],
        'y': ['y3', 'y4'],
        'val': [5.1, 6]})
    cdf.integrate_df(new_df)
    query_df = pl.DataFrame({
        'x': ['x1', 'x2', 'x3', 'x4'],
        'y': ['y1', 'y2', 'y3', 'y4']})
    result = cdf.get_df_entries(query_df)
    expected = pl.DataFrame({
        'x': ['x1', 'x2', 'x3', 'x4'],
        'y': ['y1', 'y2', 'y3', 'y4'],
        'val': [1.1, 4, 5.1, 6]})
    assert result.sort(['x', 'y']).frame_equal(
        expected.sort(['x', 'y']), null_equal=True)


def test_update_with_overlap():
    cdf = CachedMatrix()
    df = pl.DataFrame({
        'x': ['x1', 'x2'],
        'y': ['y1', 'y1'],
        'val': [1.1, 2]})
    cdf.integrate_df(df)
    overlap_df = pl.DataFrame({
        'x': ['x1'],
        'y': ['y1'],
        'val': [1.1]})
    with pytest.raises(ValueError) as e:
        cdf.integrate_df(overlap_df)
    assert "Cannot overwrite existing entries. Purge if necessary." in str(
        e.value)


def test_check_missing_entries():
    cdf = CachedMatrix()
    initial_df = pl.DataFrame({
        'x': ['x1', 'x2'],
        'y': ['y1', 'y2'],
        'val': [1.1, 2.2]})
    cdf.integrate_df(initial_df)
    check_df = pl.DataFrame({
        'x': ['x1', 'x2', 'x3'],
        'y': ['y1', 'y3', 'y2']})
    missing_entries_df = cdf.get_missing_entries(check_df)
    expected_missing_df = pl.DataFrame({
        'x': ['x2', 'x3'],
        'y': ['y3', 'y2']
    })
    assert missing_entries_df.equals(expected_missing_df)


def test_custom_colnames_integration_and_retrieval():
    colnames = {'x': 'lat', 'y': 'lon', 'val': 'height'}
    cdf = CachedMatrix(colnames=colnames)
    df = pl.DataFrame({
        'lat': ['lat1', 'lat2', 'lat1', 'lat2'],
        'lon': ['lon1', 'lon1', 'lon2', 'lon2'],
        'height': [1.1, 2, 3, 4]})
    cdf.integrate_df(df)
    query_df = pl.DataFrame({
        'lat': ['lat1', 'lat2', 'lat1', 'lat2'],
        'lon': ['lon1', 'lon1', 'lon2', 'lon2']})
    result = cdf.get_df_entries(query_df)
    expected = pl.DataFrame({
        'lat': ['lat1', 'lat1', 'lat2', 'lat2'],
        'lon': ['lon1', 'lon2', 'lon1', 'lon2'],
        'height': [1.1, 3, 2, 4]}).sort(['lat', 'lon'])

    assert result.frame_equal(
        expected, null_equal=True), "The retrieved entries do not match the expected output with custom column names."
