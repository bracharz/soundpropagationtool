# Adjust the import according to your setup
import polars as pl
import pytest
from soundpropagationtools.coordtools import *


def test_assert_lat_values_valid():
    # Test with valid latitude values
    assert_lat_values((0, 45, -45, 90, -90))  # Should pass without any error


def test_assert_lat_values_invalid():
    # Test with a latitude value out of range
    with pytest.raises(ValueError) as e:
        assert_lat_values((100,))  # Invalid latitude, should raise ValueError
    assert "Value 100 does not match expected pattern" in str(e.value)


def test_assert_lat_values_edge_cases():
    # Test edge cases for latitude validation
    assert_lat_values((-90, 90))  # Edge cases, should pass without any error


def test_assert_lon_values_valid():
    # Test with valid longitude values
    assert_lon_values((0, 45, -45, 180, -180))  # Should pass without any error


def test_assert_lon_values_invalid():
    # Test with a longitude value out of range
    with pytest.raises(ValueError) as e:
        # Invalid longitude, should raise ValueError
        assert_lon_values((-200,))
    assert "Value -200 does not match expected pattern" in str(e.value)


def test_assert_lon_values_edge_cases():
    # Test edge cases for longitude validation
    assert_lon_values((-180, 180))  # Edge cases, should pass without any error


def test_standard_range():
    tickmarks, tickdist = calculate_coord_ticks(10, (0, 100))
    expected_tickmarks = tuple(range(0, 101, 10))
    expected_tickdist = 10
    assert tickmarks == expected_tickmarks
    assert tickdist == expected_tickdist


def test_small_range():
    tickmarks, tickdist = calculate_coord_ticks(5, (0.001, 0.01))
    assert tickdist <= 0.01 / 5


def test_large_range():
    tickmarks, tickdist = calculate_coord_ticks(5, (-1000, 1000))
    assert tickmarks[0] <= -1000 and tickmarks[-1] >= 1000
    assert len(tickmarks) > 2


def test_negative_range():
    tickmarks, tickdist = calculate_coord_ticks(5, (-10, 10))
    assert tickmarks[0] <= -10
    assert tickmarks[-1] >= 10


def test_zero_span_range():
    with pytest.raises(OverflowError):
        calculate_coord_ticks(10, (5, 5))


def test_high_resolution():
    tickmarks, tickdist = calculate_coord_ticks(100, (0, 1))
    assert len(tickmarks) >= 1


def test_low_resolution():
    tickmarks, tickdist = calculate_coord_ticks(2, (0, 10))
    assert len(tickmarks) >= 2


def test_non_integer_values():
    tickmarks, tickdist = calculate_coord_ticks(10, (0.1, 1.1))
    assert tickmarks[0] >= 0.1
    assert tickmarks[-1] <= 1.1


def test_very_close_min_max():
    tickmarks, tickdist = calculate_coord_ticks(10, (0.11251, 1.23156))
    assert tickmarks[0] <= 0.11251
    assert tickmarks[-1] >= 1.23156


def test_descending_order_input():
    with pytest.raises(ValueError):
        calculate_coord_ticks(10, (100, 0))


def test_haversine_distance_np():
    # Test DataFrame
    df = pl.DataFrame({
        "lat": [52.2296756, 0, -34.603722],
        "lon": [21.0122287, 0, -58.381592]
    })
    given_point = (52.2296756, 21.0122287)  # Warsaw
    expected_dist = [0, 6129, 12327]  # Approx distances in kilometers
    result_df = haversine_distance(df, given_point)
    np.testing.assert_allclose(
        result_df['dist'].to_numpy(), expected_dist, atol=4)
