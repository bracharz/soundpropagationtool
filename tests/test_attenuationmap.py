import pytest
import importlib
import numpy as np
from soundpropagationtools import attenuationmap
from soundpropagationtools.attenuationmap import AttenuationMap, BaseAttenuationMap
importlib.reload(attenuationmap)


def test_attenuation_basemap():
    bam = BaseAttenuationMap(
        soundorigin=(0, 0),
        min_resolution=(10, 10),
        viewport=(0, 1, 0, 1),
        baselevel=20.1)
    mat = bam.get_attenuationmatrix()
    assert np.array_equal(mat, np.full((11, 11), 20.1))
