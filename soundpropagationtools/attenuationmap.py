# find out how to plot a heatmap with predefined values for specified colours on top of a map in python

from abc import ABC, abstractmethod
import numpy as np
import polars as pl
from itertools import product
from soundpropagationtools.coordtools import calculate_coord_ticks
from soundpropagationtools.cachedmatrix import CachedMatrix


class AttenuationMap(ABC):
    def __init__(self, soundorigin, min_resolution, viewport):
        self.soundorigin = soundorigin  # lat,lon
        self.min_resolution = min_resolution  # pixels_width, pixels_height
        self.viewport = viewport  # min_lon,max_lon,min_lat,max_lat,
        self.axes = None  # 2 tuples, axes for lon and lat matrix
        self.cacheddf = CachedMatrix()  # CachedDF of cached values
        self.attenuationmatrix = None  # (n,d) matrix
        self.update_viewport()

    @property
    def soundorigin(self):
        return self._soundorigin

    @soundorigin.setter
    def soundorigin(self, value):
        if not isinstance(value, tuple) or len(value) != 2:
            raise ValueError("Sound origin must be a tuple with 2 values.")
        self._soundorigin = value

    @property
    def viewport(self):
        return self._viewport

    @viewport.setter
    def viewport(self, value):
        if not isinstance(value, tuple) or len(value) != 4:
            raise ValueError("Viewport must be a tuple with 4 values.")
        self._viewport = value

    def update_viewport(self):
        lon_ticks = calculate_coord_ticks(
            self.min_resolution[0], (self.viewport[0], self.viewport[1]))
        lat_ticks = calculate_coord_ticks(
            self.min_resolution[1], (self.viewport[2], self.viewport[3]))
        self.axes = (lon_ticks[0], lat_ticks[0])

    @abstractmethod
    def retrieveAttenuationData(self, df):
        pass

    @abstractmethod
    def calculateAttenuationMatrix(self):
        pass

    def get_attenuationmatrix(self):
        combinations = list(product(self.axes[0], self.axes[1]))
        request_df = pl.DataFrame(combinations, schema={
            'x': pl.Utf8, 'y': pl.Utf8})
        missing_entries = self.cacheddf.get_missing_entries(request_df)
        if missing_entries.height != 0:
            new_df = self.retrieveAttenuationData(missing_entries)
            self.cacheddf.integrate_df(new_df)
        self.attenuationmatrix = df2mat(
            self.cacheddf.get_df_entries(request_df))
        return self.attenuationmatrix


def df2mat(df):
    pivot_df = df.pivot(index='x', columns='y', values='val').fill_null(0)
    value_columns = pivot_df.columns[1:]
    matrix_no_header_no_index = pivot_df.select(
        value_columns).to_numpy().tolist()
    return matrix_no_header_no_index


class BaseAttenuationMap(AttenuationMap):
    def __init__(self, soundorigin, min_resolution, viewport, baselevel):
        super().__init__(soundorigin, min_resolution, viewport)
        self.baselevel = baselevel

    @property
    def baselevel(self):
        return self._baselevel

    @baselevel.setter
    def baselevel(self, value):
        self._baselevel = value

    def retrieveAttenuationData(self, df):
        return df.with_columns(pl.lit(self.baselevel).cast(pl.Float64).alias('val'))

    def calculateAttenuationMatrix(self):
        self.attenuationmatrix = np.full(
            (len(self.axes[0]), len(self.axes[1])),
            self.baselevel)


class BaseAttenuationMap(AttenuationMap):
    def __init__(self, soundorigin, min_resolution, viewport, baselevel):
        super().__init__(soundorigin, min_resolution, viewport)

    def retrieveAttenuationData(self, df):
        return df.with_columns(pl.lit(self.baselevel).cast(pl.Float64).alias('val'))

    def calculateAttenuationMatrix(self):
        self.attenuationmatrix = np.full(
            (len(self.axes[0]), len(self.axes[1])),
            self.baselevel)
