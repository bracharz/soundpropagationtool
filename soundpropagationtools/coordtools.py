import math
import numpy as np
import polars as pl


def calculate_coord_ticks(min_res, viewport_axis):
    """
    Generates rounded tick marks and their distance for a given axis range, addressing floating-point precision issues.
    Ensures the inclusion of the first rounded tick below the minimum value and the last tick above the maximum value if necessary.

    Args:
        min_res (int): The minimum number of tick marks desired within the axis range.
        viewport_axis (tuple): The (min_val, max_val) range of the axis.

    Returns:
        tuple: Rounded tick marks within the given axis range.
        float: The distance between consecutive tick marks, rounded to a sensible value.
    """
    min_val, max_val = viewport_axis
    axis_span = max_val - min_val

    # Determine a preliminary tick distance
    preliminary_tick_dist = axis_span / min_res

    # Round the tick distance to the nearest power of 10
    tickdist = 10 ** round(np.log10(preliminary_tick_dist))

    # Determine the number of decimal places for rounding, based on tickdist
    decimal_places = max(-int(np.floor(np.log10(tickdist))), 0)

    # Calculate the start and end points for tick marks
    start_point = np.floor(min_val / tickdist) * tickdist
    end_point = np.ceil(max_val / tickdist) * tickdist

    # Generate and round tick marks
    tickmarks = np.arange(
        start=start_point, stop=end_point + tickdist, step=tickdist)
    tickmarks = np.round(tickmarks, decimal_places)
    return tuple(tickmarks), tickdist


def assert_lat_values(lat_values):
    for lat in lat_values:
        if not -90 <= lat <= 90:
            raise ValueError(
                f"Value {lat} does not match expected pattern: must be between -90 and 90 degrees inclusive.")


def assert_lon_values(lon_values):
    for lon in lon_values:
        if not -180 <= lon <= 180:
            raise ValueError(
                f"Value {lon} does not match expected pattern: must be between -180 and 180 degrees inclusive.")


def haversine_distance(df, given_point):
    # Unpack the given point and convert to radians
    lat2, lon2 = np.radians(given_point)

    # Convert DataFrame lat/lon columns from degrees to radians
    lat1 = np.radians(df['lat'].to_numpy())
    lon1 = np.radians(df['lon'].to_numpy())

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2)**2
    c = 2 * np.arcsin(np.sqrt(a))
    r = 6371  # Earth radius in kilometers
    dist = c * r
    # Create the Series without specifying a name here
    dist_series = pl.Series(dist)
    new_df = df.with_columns(dist_series.alias('dist'))
    return new_df
