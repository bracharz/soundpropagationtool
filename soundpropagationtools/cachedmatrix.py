import polars as pl


class CachedMatrix:
    def __init__(self, colnames=None, df=None):
        # Default column names
        default_colnames = {'x': 'x', 'y': 'y', 'val': 'val'}
        self.colnames = colnames if colnames is not None else default_colnames

        # Adjusted schema to use provided column names
        self.schema = {self.colnames['x']: pl.Utf8,
                       self.colnames['y']: pl.Utf8,
                       self.colnames['val']: pl.Float64}

        self.df = pl.DataFrame(schema=self.schema)
        if df is not None:
            self.integrate_df(df)

    def validate_request_df(self, df):
        subset_schema = {key: self.schema[key] for key in [
            self.colnames['x'], self.colnames['y']]}
        if not all(column in df.columns for column in subset_schema.keys()):
            raise ValueError(
                f"New DataFrame must contain columns '{self.colnames['x']}' and '{self.colnames['y']}'")
        for col, dtype in subset_schema.items():
            if df[col].dtype != dtype:
                raise ValueError(f"Column '{col}' must be of type {dtype}")

    def validate_input_df(self, df):
        if not all(column in df.columns for column in self.schema.keys()):
            raise ValueError(
                f"New DataFrame must contain columns '{self.colnames['x']}', '{self.colnames['y']}', and '{self.colnames['val']}'")
        for col, dtype in self.schema.items():
            if df[col].dtype != dtype:
                raise ValueError(f"Column '{col}' must be of type {dtype}")

    def integrate_df(self, df):
        self.validate_input_df(df)
        if self.df.shape[0] > 0 and not self.df.join(df, on=[self.colnames['x'], self.colnames['y']], how='inner').is_empty():
            raise ValueError(
                "Cannot overwrite existing entries. Purge if necessary.")
        self.df = pl.concat([self.df, df]).unique(
            subset=[self.colnames['x'], self.colnames['y']])

    def get_df_entries(self, df):
        self.validate_request_df(df)
        matching_entries = df.join(
            self.df, on=[self.colnames['x'], self.colnames['y']], how='inner')
        if matching_entries.shape[0] != df.shape[0]:
            raise ValueError("Missing entries, please integrate these first.")
        return matching_entries.sort([self.colnames['x'], self.colnames['y']])

    def purge_df(self):
        self.df = pl.DataFrame(schema=self.schema)

    def get_missing_entries(self, df):
        self.validate_request_df(df)
        missing_entries = df.join(
            self.df, on=[self.colnames['x'], self.colnames['y']], how='anti').select([self.colnames['x'], self.colnames['y']])
        return missing_entries
